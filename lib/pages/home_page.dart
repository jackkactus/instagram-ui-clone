import 'package:flutter/material.dart';
import 'package:instagram_ui_clone/data/data.dart';
import 'package:instagram_ui_clone/widgets/home_page_widgets/home_app_bar.dart';
import 'package:instagram_ui_clone/widgets/home_page_widgets/story_card.dart';
import 'package:instagram_ui_clone/widgets/home_page_widgets/post_card.dart';
import 'package:instagram_ui_clone/widgets/home_page_widgets/add_story_card.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        const CloneHomeAppBar(),
        SliverToBoxAdapter(
          child: Column(
            children: [
              Container(
                color: Colors.white,
                height: 105,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    //itemCount: stories.length + 1,
                    itemCount: 2,
                    itemBuilder: (context, index) {
                      return Container(
                        width: 90,
                        padding: const EdgeInsets.symmetric(vertical: 5.0),
                        child: index == 0 ? const AddStoryCard() : StoryCard(story: stories[index-1],),
                      );
                    }),
              ),
              const Divider(
                height: 1,
              )
            ],
          ),
        ),
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index){
              return PostCard(post: post[index],);
            },
            childCount: post.length
            )
        )
      ],
    );
  }
}
