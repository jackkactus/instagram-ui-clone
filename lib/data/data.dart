import 'package:instagram_ui_clone/models/user.dart';
import 'package:instagram_ui_clone/models/post.dart';
import 'package:instagram_ui_clone/models/story.dart';

User currentUser = User('id', 'lalalalisa_m',
    'https://i.pinimg.com/736x/e7/ce/20/e7ce20822310c290bee0a615135b0e89.jpg');

final List<Story> stories = [
  Story('id', currentUser),
  Story('id', currentUser),
  Story('id', currentUser),
  Story('id', currentUser),
  Story('id', currentUser),
  Story('id', currentUser),
];

final List<Post> post = [
  Post(
      'id',
      currentUser,
      'https://instagram.fsgn13-4.fna.fbcdn.net/v/t51.2885-15/315104456_496593175863007_3586903640244513878_n.jpg?stp=dst-jpg_e35_p640x640_sh0.08&_nc_ht=instagram.fsgn13-4.fna.fbcdn.net&_nc_cat=107&_nc_ohc=MvF7GUD5R8IAX-BsxGw&edm=ALQROFkBAAAA&ccb=7-5&ig_cache_key=Mjk2ODIwNjM1MDkwMzk2MDkzMg%3D%3D.2-ccb7-5&oh=00_AfBDFzq-BppuhsuOq-79EM2wy2hZvLZHNgfF0JdD5My2yw&oe=638C286B&_nc_sid=30a2ef',
      'title',
      'Bien Hoa, Vietnam',
      '  @datsnobiety',
      '5 minutes ago',
      '1.131.236',
      '87.539',
      true,
      true),
  Post(
      'id',
      currentUser,
      'https://instagram.fsgn4-1.fna.fbcdn.net/v/t51.2885-15/316677086_694085325328987_2215904140025550424_n.jpg?stp=dst-jpg_e35_p640x640_sh0.08&_nc_ht=instagram.fsgn4-1.fna.fbcdn.net&_nc_cat=1&_nc_ohc=nSnQsHj660oAX-lFOjA&edm=ALQROFkBAAAA&ccb=7-5&ig_cache_key=Mjk3ODU4ODg1NDY5MDE3ODI5OA%3D%3D.2-ccb7-5&oh=00_AfAuY65hElnyBsvTikLyBJ6VVEZxrqrF2lL8hrpSSBEzaQ&oe=638B6773&_nc_sid=30a2ef',
      'title',
      'Tokyo, Japan',
      'caption',
      'a few seconds ago',
      '32',
      '3',
      false,
      false),
  Post(
      'id',
      currentUser,
      'https://instagram.fsgn4-1.fna.fbcdn.net/v/t51.2885-15/305669426_113154861530117_7645406109541373520_n.jpg?stp=dst-jpg_e35_p640x640_sh0.08&_nc_ht=instagram.fsgn4-1.fna.fbcdn.net&_nc_cat=105&_nc_ohc=yccsjcMffkoAX8zoWD9&tn=NHHNHP_BKNcRXsCW&edm=ALQROFkBAAAA&ccb=7-5&ig_cache_key=MjkyMzM4OTIwNjQ3NjYwMjA0OQ%3D%3D.2-ccb7-5&oh=00_AfDf4naY0-E742YS2O5VHNVnZ82BbIw9QnZs3vbYXF7Agg&oe=638D0D9F&_nc_sid=30a2ef',
      'title',
      'Bien Hoa',
      'caption',
      '3 hours ago',
      '12',
      '1',
      true,
      false),
  Post(
      'id',
      currentUser,
      'https://instagram.fsgn8-2.fna.fbcdn.net/v/t51.2885-15/314738565_1293622881462468_8301944969041687374_n.jpg?stp=dst-jpg_e35_p640x640_sh0.08&_nc_ht=instagram.fsgn8-2.fna.fbcdn.net&_nc_cat=100&_nc_ohc=NBU6sTAEOs4AX93UhPN&edm=ALQROFkBAAAA&ccb=7-5&ig_cache_key=Mjk2ODIwNjM1MTAyOTc4MDI3MA%3D%3D.2-ccb7-5&oh=00_AfA8wPDOaAXHh7eYwjvwl6nXqNlYDS6BklgRjgA-fD3FwA&oe=638CD498&_nc_sid=30a2ef',
      'title',
      'Beijing, China',
      'caption',
      'a day ago',
      '1234',
      '80',
      false,
      false),
];
