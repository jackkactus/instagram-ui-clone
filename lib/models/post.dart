import 'package:instagram_ui_clone/models/user.dart';

class Post {
  final String id;
  final User postBy;
  final String imgUrl;
  final String title;
  final String location;
  final String caption;
  final String postedTime;
  final String totalLike;
  final String totalComment;
  final bool isLike;
  final bool isBookmark;

  Post(
      this.id,
      this.postBy,
      this.imgUrl,
      this.title,
      this.location,
      this.caption,
      this.postedTime,
      this.totalLike,
      this.totalComment,
      this.isLike,
      this.isBookmark);
}
