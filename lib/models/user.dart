class User {
  final String id;
  final String username;
  final String userAvatarUrl;

  User(this.id, this.username, this.userAvatarUrl);
}