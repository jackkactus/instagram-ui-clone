import 'package:instagram_ui_clone/models/user.dart';

class Story {
  final String id;
  final User postBy;

  Story(this.id, this.postBy);
}