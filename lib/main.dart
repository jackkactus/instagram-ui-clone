import 'package:flutter/material.dart';
import 'package:instagram_ui_clone/pages/explore_page.dart';
import 'package:instagram_ui_clone/pages/home_page.dart';
import 'package:instagram_ui_clone/pages/profile_page.dart';
import 'package:instagram_ui_clone/pages/reel_page.dart';
import 'package:instagram_ui_clone/widgets/bottom_navigation_bar.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Instagram Clone',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity
      ),
      home: const NavigationContainer(),
    );
  }
}

class NavigationContainer extends StatefulWidget {
  const NavigationContainer({Key? key}) : super(key: key);

  @override
  State<NavigationContainer> createState() => _NavigationContainerState();
}

class _NavigationContainerState extends State<NavigationContainer> {
  int _selectedPageIndex = 0;
  static const List<Widget> _appPages = [
    HomePage(),
    ExplorePage(),
    ReelPage(),
    ProfilePage(),
  ];

  void _onIconTap(int index){
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _appPages[_selectedPageIndex],
      ),
      bottomNavigationBar: CloneBottomNavigationBar(
        selectedPageIndex: _selectedPageIndex,
        onIconTap: _onIconTap,
      ),
    );
  }
}

