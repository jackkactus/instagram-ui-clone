import 'package:flutter/material.dart';
import 'package:instagram_ui_clone/models/post.dart';

class PostCard extends StatelessWidget {
  const PostCard({Key? key, required this.post}) : super(key: key);

  final Post post;

  final double _iconSize = 32;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            title: Text(
              post.postBy.username,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              post.location,
              style: const TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  fontSize: 13
              ),
            ),
            leading: CircleAvatar(
              backgroundImage: NetworkImage(post.postBy.userAvatarUrl),
            ),
            trailing: const Icon(Icons.more_horiz, color: Colors.black,),
          ),
          Image.network(
            post.imgUrl,
            height: 320,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () =>{},
                  icon: Icon(post.isLike ? Icons.favorite : Icons.favorite_outline),
                  iconSize: _iconSize,
                  color: post.isLike ? Colors.red : Colors.black,
                ),
                IconButton(
                  onPressed: () =>{},
                  icon: const Icon(Icons.chat_bubble_outline_rounded),
                  iconSize: _iconSize,
                ),IconButton(
                  onPressed: () =>{},
                  icon: const Icon(Icons.send_outlined),
                  iconSize: _iconSize,
                ),
                Spacer(),
                IconButton(
                  onPressed: () =>{},
                  icon: Icon(post.isBookmark ? Icons.bookmark : Icons.bookmark_outline),
                  iconSize: _iconSize,
                )
              ],
            ),
          ),
          const SizedBox(height: 5,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '${post.totalLike} likes',
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                  ),
                ),
                const SizedBox(height: 5,),
                RichText(
                  text: TextSpan(
                    text: post.postBy.username,
                    style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                    children: [
                      TextSpan(
                        text: post.caption,
                        style: const TextStyle(fontWeight: FontWeight.normal, color: Colors.blue)
                      )
                    ]
                  )
                ),
                const SizedBox(height: 5,),
                Text('View all ${post.totalComment} comments', style: const TextStyle(color: Colors.grey),),
                const SizedBox(height: 5,),
                Text(post.postedTime, style: const TextStyle(color: Colors.grey, fontSize: 13),)
              ],
            ),
          )
        ],
      ),
    );
  }
}
