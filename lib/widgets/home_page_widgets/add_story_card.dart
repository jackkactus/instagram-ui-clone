import 'package:flutter/material.dart';
import 'package:instagram_ui_clone/data/data.dart';

class AddStoryCard extends StatelessWidget {
  const AddStoryCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            const CircleAvatar(
              backgroundColor: Colors.black,
              radius: 32,
              backgroundImage: NetworkImage(
                  'https://scontent.fsgn3-1.fna.fbcdn.net/v/t39.30808-6/305479341_2918220705148502_6135668791378143355_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=B8zNVB-SJwIAX9-t71i&_nc_ht=scontent.fsgn3-1.fna&oh=00_AfD2smOj_FnSCJNfWnvHG8337iceOl92a3SIFp4BtIqkVQ&oe=638CFFD8'),
            ),
            Positioned(
              top: 42,
              left: 40,
              child: Container(
                height: 25,
                width: 25,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 3),
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.blue),
                child: IconButton(
                  padding: EdgeInsets.zero,
                  onPressed: () => {},
                  icon: const Icon(Icons.add),
                  iconSize: 20,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
        const Spacer(),
        const Text('Your Story',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.grey))
      ],
    );
  }
}
