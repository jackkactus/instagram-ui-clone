import 'package:flutter/material.dart';
import 'package:instagram_ui_clone/data/data.dart';

class CloneBottomNavigationBar extends StatelessWidget {
  const CloneBottomNavigationBar({Key? key, required this.selectedPageIndex, required this.onIconTap}) : super(key: key);

  final int selectedPageIndex;
  final Function onIconTap;
  final double _iconSize = 30;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Row(
        mainAxisAlignment:  MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            iconSize: _iconSize,
            icon: Icon(selectedPageIndex == 0 ? Icons.home : Icons.home_outlined),
            onPressed: () => {onIconTap(0)},
          ),
          IconButton(
            iconSize: _iconSize,
            icon: Icon(selectedPageIndex == 1 ? Icons.search : Icons.search_outlined),
            onPressed: () => {onIconTap(1)},
          ),
          IconButton(
              iconSize: _iconSize,
              icon: Icon(selectedPageIndex == 2 ? Icons.smart_display : Icons.smart_display_outlined),
              onPressed: () => {onIconTap(2)},
          ),
          IconButton(
            iconSize: _iconSize,
            icon: Icon(selectedPageIndex == 2 ? Icons.favorite : Icons.favorite_outline),
            onPressed: () => {onIconTap(3)},
          ),
          InkWell(
            onTap: () => {onIconTap(4)},
            child: CircleAvatar(
              backgroundColor:  Colors.black, radius: _iconSize/2,
              backgroundImage: const NetworkImage('https://scontent.fsgn3-1.fna.fbcdn.net/v/t39.30808-6/305479341_2918220705148502_6135668791378143355_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=B8zNVB-SJwIAX9-t71i&_nc_ht=scontent.fsgn3-1.fna&oh=00_AfD2smOj_FnSCJNfWnvHG8337iceOl92a3SIFp4BtIqkVQ&oe=638CFFD8'),
            ),
          )
        ],
      ),
    );
  }
}
